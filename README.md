[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=version&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fstarwarsffg%2F-%2Fraw%2Fmaster%2Fstarwarsffg_pt-BR%2Fmodule.json)](https://gitlab.com/foundryvtt-pt-br/starwarsffg) [![Min Core](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Min%20Core&prefix=v&query=minimumCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fstarwarsffg%2F-%2Fraw%2Fmaster%2Fstarwarsffg_pt-BR%2Fmodule.json)](http://foundryvtt.com/) [![Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fstarwarsffg%2F-%2Fraw%2Fmaster%2Fstarwarsffg_pt-BR%2Fmodule.json)](http://foundryvtt.com/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&logoColor=white)](https://discordapp.com/invite/DDBZUDf) [![Translated](https://img.shields.io/endpoint?url=https://l10n.creativelabs.dev/star-wars-ffg)](https://www.transifex.com/foundryvtt-brasil/star-wars-ffg/)


# FoundryVTT Star Wars FFG Brazilian Portuguese

## Português 
Esse módulo adiciona o idioma Português (Brasil) como uma opção a ser selecionada nas configurações do FoundryVTT. 
Selecionar essa opção traduzirá vários aspectos do sistema Star Wars FFG.
Esse módulo traduz somente aspectos relacionados ao sistema de [Star Wars FFG](https://github.com/StarWarsFoundryVTT/StarWarsFFG). 
Esse módulo não traduz outras partes do software FoundryVTT, como a interface principal. 
Para isso, confira o módulo [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

### Instalação por Manifesto
Na opção Add-On Modules clique em Install Module e coloque o seguinte link no campo Manifest URL

`https://gitlab.com/foundryvtt-pt-br/starwarsffg/-/raw/master/starwarsffg_pt-BR/module.json`

### Instalação Manual
Se as opções acima não funcionarem, faça o download do arquivo [starwarsffg-pt-br-master.zip](https://gitlab.com/foundryvtt-pt-br/starwarsffg/-/archive/master/starwarsffg-pt-br-master.zip?path=starwarsffg_pt-BR) e extraia a pasta `starwarsffg_pt-BR` dentro da pasta `Data/modules`
Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.


## English
This module adds the Portuguese (Brazil) language as an option to be selected in the FoundryVTT settings. 
Selecting this option will translate various aspects of the Star Wars FFG system. 
This module only translates aspects related to the [Star Wars FFG](https://github.com/StarWarsFoundryVTT/StarWarsFFG) system. 
This module does not translate other parts of the FoundryVTT software, such as the main interface. 
For this, check out the module [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

### Installation by Manifest
In the Add-On Modules option click on Install Module and place the following link in the Manifest URL field

`https://gitlab.com/foundryvtt-pt-br/starwarsffg/-/raw/master/starwarsffg_pt-BR/module.json`

### Manual Installation
If the above options don't work, download the file [starwarsffg-pt-br-master.zip](https://gitlab.com/foundryvtt-pt-br/starwarsffg/-/archive/master/starwarsffg-pt-br-master.zip?path=starwarsffg_pt-BR) and extract the `starwarsffg_pt-BR` folder inside the `Data/modules` folder
Once this is done, activate the module in the settings of the world where you intend to use it and then change the language in the settings.